
import './App.css';
import React, { Component , useRef} from 'react';
import StockList from './Components/StockList';
import SubmitButton from './Components/SubmitButton';
import Demoref from './Components/Demoref';





function App() {
  const ref = useRef();
  return (
    <div className="App">
       <StockList></StockList>
        <SubmitButton/>
        <Demoref/> 
    </div>
  );
}

export default App;

